<% 
' Arquivo de Configura��o
' =======================
' Vari�veis do Sistema
	'On Error Resume Next	
	Session.LCID = 1046
	Response.Buffer = true
	Response.Expires = 0
	Response.CacheControl = "no-cache"
	Response.AddHeader "Pragma", "no-cache"
	Server.ScriptTimeOut = 9999

' Vari�veis Gerais
	Dim varTitulo,varVersao,varMesAtual,varAnoAtual,ConnectString,Conn,varSQL,dbTabela,SQL,pageTitulo
	varTitulo = "fisCal Live"
	varVersao = "1.0"
	varMesAtual = Month(Date)
	varAnoAtual = Year(Date)
	varDataAtual = Day(Date) & "/" & Month(Date) & "/" & Year(Date)
	
' Fun��es
	Public Function dbConecta()
		ConnectString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & Server.MapPath("fldb.mdb")
		Set Conn = Server.CreateObject("ADODB.Connection")
		Conn.Open ConnectString
	End Function

	Public Function dbDesconecta()
		Conn.Close
		Set Conn = Nothing
	End Function
	
	Public Function dbAbreTabela(varSQL)
		Set dbTabela = Server.CreateObject("ADODB.RecordSet")
		SQL = varSQL
		dbTabela.Open SQL, Conn, 3, 3
	End Function
	
	Public Function dbFechaTabela()
		dbTabela.Close
		Set dbTabela = Nothing
	End Function

	pageTitulo = varTitulo & " v" & varVersao
%>