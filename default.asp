<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<!--#include file="configuracao.asp"-->
<%
' P�gina Principal
' ================
' Declara��o de Variaveis Locaos
	varMesSelecionado = Trim(Request.QueryString("mes"))
	If Len(varMesSelecionado) = 0 Then
		varMesCorrente = varMesAtual
	Else
		varMesCorrente = CInt(varMesSelecionado)
	End If
	varEfetuaPG = Trim(Request.QueryString("efetuapg"))
%>
<%
	If Len(varEfetuaPG) <> 0 Then
		dbConecta()
		dbAbreTabela("UPDATE contas SET situacao='pg' WHERE id=" & varEfetuaPG & "")
		dbFechaTabela()
		dbDesconecta()
	End If
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><%= pageTitulo %></title>
<style type="text/css">
<!--
.tabelaTitulo {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	font-weight: bold;
}
.tabelaCelula {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
}
-->
</style>
<script language="JavaScript" type="text/JavaScript"><!--
	function MM_goToURL() { //v3.0
		var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
		for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
	}
//--></script>
</head>

<body>
<h1><%= pageTitulo %></h1>
<table width="900" border="0" cellspacing="0" cellpadding="5" align="center" bgcolor="#F5F5F5">
	<tr>
<%
	If varMesCorrente > 1 Then
		Response.Write("<td colspan='" & varMesCorrente & "' align='right' bgcolor='#CCCCCC' class='tabelaTitulo'>Contas referentas &agrave;: </td>")
	Else
		Response.Write("<td colspan='2' align='right' bgcolor='#CCCCCC' class='tabelaTitulo'>Contas referentas &agrave;: </td>")
	End IF
%>
	</tr>
	<tr>
<%
	For xMes = 1 to 12
		If xMes = varMesCorrente Then
			colorMesAtual = "bgcolor='#CCCCCC'"
		Else
			colorMesAtual = "bgcolor='#F5F5F5'"
		End If
		Response.Write("<td width='65' align='center' " & colorMesAtual & " class='tabelaCelula'><a href='?mes="&xMes&"'>" & xMes & "/" & varAnoAtual & "</a></td>")
	Next
%>
</table>
<table width="900" border="0" cellspacing="0" cellpadding="5" align="center">
	<tr>
		<td width="15" class="tabelaTitulo" align="center" bgcolor="#33CCFF">PG</td>
		<td width="675" class="tabelaTitulo" align="center" bgcolor="#33CCFF">NOME</td>
		<td width="25" class="tabelaTitulo" align="center" bgcolor="#33CCFF">PARCELA</td>
		<td width="80" class="tabelaTitulo" align="center" bgcolor="#33CCFF">VENCIMENTO</td>
		<td width="80" class="tabelaTitulo" align="center" bgcolor="#33CCFF">VALOR</td>
	</tr>
<%
	dbConecta()
	dbAbreTabela("SELECT * FROM contas WHERE mesref=" & varMesCorrente & " ORDER BY conta ASC")
	While not dbTabela.EOF
		If FormatDateTime(dbTabela("datavenc")) < FormatDateTime(varDataAtual) and dbTabela("situacao") = "npg" Then
			colorTable = "bgcolor='#FF0000'"
		Else
			colorTable = "bgcolor='#FFFFFF'"
		End If
		Response.Write("<tr>")
		If dbTabela("situacao") = "pg" Then
			Response.Write("	<td align='center' bgcolor='#336699' " & colorTable & "><input name='radiobutton' type='radio' value='radiobutton' disabled /></td>")
		Else
			Response.Write("	<td align='center' bgcolor='#336699' " & colorTable & "><input name='radiobutton' type='radio' value='radiobutton' onClick="&Chr(34)&"MM_goToURL('parent','?mesref="&varMesCorrente&"&efetuapg="&dbTabela("id")&"');return document.MM_returnValue"&Chr(34)&" /></td>")
		End If
		Response.Write("	<td class='tabelaCelula' " & colorTable & ">" & dbTabela("conta") & "</td>")
		Response.Write("	<td class='tabelaCelula' align='center' " & colorTable & ">" & dbTabela("parcelaatual") & "/" & dbTabela("parcelaqdt") & "</td>")
		Response.Write("	<td class='tabelaCelula' align='center' " & colorTable & ">" & dbTabela("datavenc") & "</td>")
		Response.Write("	<td class='tabelaCelula' align='right' " & colorTable & ">" & FormatCurrency(dbTabela("valor")) & "</td>")
		Response.Write("</tr>")
	dbTabela.MoveNext
	Wend
	If dbTabela.BOF Then
		Response.Write("<tr>")
		Response.Write("	<td colspan='2' class='tabelaCelula' align='center'> Nenhuma conta cadastrada referente a este m�s! </td>")
		Response.Write("</tr>")
	End If

	dbFechaTabela()
	dbDesconecta()
%>
</table>
</body>
</html>
<%
	
%>